// const profilValues = ["jenisKelamin", "usia", "tglLahir", "pendidikan", "pengeluaran", "pekerjaan", "agama", "geografi", "ring"];
// const profileMappings = {
//     agama: ["Budha", "Hindu", "Islam", "Katolik", "Konghucu", "Kristen", "LainLain"],
//     pengeluaran: ["1", "2", "3", "4", "5", "6", "7"],
//     ring: ["0", "1", "2", "3", "4"],
//     pekerjaan: ["2", "33", "20", "15", "13", "28", "30", "7", "14", "25", "19", "9", "16", "23", "5", "4", "12", "29", "3", "34", "21", "11", "31", "8", "18", "32", "24", "10", "26", "17", "6", "27", "35", "1", "22"],
//     pendidikan: ["TidakTamatSd", "Sd", "Smp", "Smu", "AkademiDiploma", "Sarjana", "PascaSarjana", "TidakBelumSekolah"],
// };

// function combineProfileValues(pValues, index = 0, current = {}) {
//     if (index === pValues.length) return [current];
//     let currentKey = pValues[index];
//     let currentValues = profileMappings[currentKey] || [];
//     let allCombinations = [];

//     for (let value of currentValues) {
//         let next = { ...current };
//         next[currentKey] = value;
//         allCombinations.push(...combineProfileValues(pValues, index + 1, next));
//     }

//     return allCombinations;
// }

// const allProfileCombinations = combineProfileValues(profilValues);

// allProfileCombinations.slice(0, 10).forEach((combination, index) => {
//     let output = {
//         input: {
//             dataSource: "h1Pemilik",
//             id: "b5d7b258-1ac0-45a6-dd22-08db93eb462a",
//             maxLeads: 10,
//             maxLeadsPerDealer: null,
//             profileFilterOrigin: {
//                 sumberData: {
//                     basisData: "h1Pemilik",
//                     maxLeads: 10,
//                     maxLeadsPerDealer: null,
//                     pilihdata: "LOL",
//                 },
//                 dealer: {
//                     dealerGroup: ["be9f9f5c-9840-ed11-a9b8-8038fbe10c2f"],
//                     region: ["7aa69f5c-9840-ed11-a9b8-8038fbe10c2f"],
//                     kabupaten: ["bdc4524a-9840-ed11-a9b8-8038fbe10c2f"],
//                     dealer: ["b1a39f5c-9840-ed11-a9b8-8038fbe10c2f"],
//                 },
//                 filterTanggal: {
//                     values: [
//                         {
//                             jenisFilter: "tanggalPembelian",
//                             logic: null,
//                             periodeTanggal: ["2023-08-01", "2023-08-03"],
//                         },
//                     ],
//                 },
//                 dataProfil: {
//                     profilValue: profilValues,
//                     values: {
//                         agama: null,
//                         geografi: {
//                             provinsi: ["8bfa5044-9840-ed11-a9b8-8038fbe10c2f"],
//                             kecamatan: ["d29d662a-d04a-ed11-a9bc-8038fbe10c2f"],
//                             kabupaten: ["9405f315-d04a-ed11-a9bc-8038fbe10c2f"],
//                         },
//                         jenisKelamin: null,
//                         pekerjaan: null,
//                         pendidikan: null,
//                         pengeluaran: null,
//                         tglLahir: ["1990-02-01", "1998-03-05"],
//                         usia: {
//                             first: 20,
//                             last: 60,
//                         },
//                         ring: null,
//                     },
//                 },
//                 metodePembelian: {
//                     statusProfil: "Sudah Lunas",
//                     values: {
//                         tenorAwal: 100,
//                         tenorAkhir: 200,
//                         dpAwal: 2500000,
//                         dpAkhir: null,
//                         cicilanAwal: 500000,
//                         cicilanAkhir: 3000000,
//                         metode: ["Cash", "Credit"],
//                         fincoy: [
//                             "PT. FEDERAL INTERNATIONAL FINANCE",
//                             "PT ADIRA DINAMIKA MULTIFINANCE TBK",
//                         ],
//                         statusPelunasan: {
//                             sudahLunas: 36,
//                         },
//                     },
//                 },
//                 informasiTambahan: {
//                     marketSegmen: ["AT HIGH", "AT LOW"],
//                     series: [
//                         "ADV_150_SERIES",
//                         "ADV_160_SERIES",
//                         "FORZA_SERIES",
//                         "GENIO_SERIES",
//                         "BEAT_SERIES",
//                     ],
//                     tipeUnit: [
//                         "3935e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "3b35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "3a35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "3d35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "3c35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "aeb0e533-eb49-ed11-a9bb-8038fbe00b9f",
//                         "3e35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "4035e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "3f35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "5235e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "4f35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "5035e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "4d35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "4e35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "4b35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "4c35e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "4935e079-aa40-ed11-a9b8-8038fbe10c2f",
//                         "5135e079-aa40-ed11-a9b8-8038fbe10c2f",
//                     ],
//                     tahun: "2018",
//                     unit: 2,
//                     jenisRo: "MainDealer",
//                 },
//                 detailPembelian: {
//                     jenisKonsumen: ["C", "G", "I", "J"],
//                     metodePembelian: null,
//                     konsumenAfPf: null,
//                     salesAbility: null,
//                     pembelianUnit: null,
//                     frekuensiServis: null,
//                     tahunRakit: null,
//                     lengkapKPB: null,
//                     lastToj: null,
//                     lastMaterialCategory: null,
//                     rataPembelianAwal: null,
//                     rataPembelianAkhir: null,
//                 },
//             },
//         },
//     };

//     profilValues.forEach(key => {
//         if (combination[key]) {
//             output.input.profileFilterOrigin.dataProfil.values[key] = combination[key];
//         } else {
//             output.input.profileFilterOrigin.dataProfil.values[key] = null;
//         }
//     });

//     console.log("Called combineProfileValues with index:", index);
//     console.log(JSON.stringify(output, null, 4));
// });
// console.log(combineProfileValues(profilValues));





// function getCombinations(arr) {
//     let result = [];
//     let f = function(prefix, arr) {
//         for (let i = 0; i < arr.length; i++) {
//             result.push(prefix.concat(arr[i]));
//             f(prefix.concat(arr[i]), arr.slice(i + 1));
//         }
//     }
//     f([], arr);
//     return result;
// }

// profilValues.forEach(profile => {
//     let values = profileMappings[profile];
//     if (!values) return; // Skip profile values that don't exist in the mappings
//     let combinations = getCombinations(values);
    
//     combinations.forEach(combination => {
//         let output = {
//             input: {
//                 dataSource: "h1Pemilik",
//                 // ... other static data
//                 profileFilterOrigin: {
//                     // ... other static data within profileFilterOrigin
//                     dataProfil: {
//                         profilValue: profilValues,
//                         values: {}
//                     },
//                 },
//             },
//         };

//         profilValues.forEach(p => {
//             if (p === profile) {
//                 output.input.profileFilterOrigin.dataProfil.values[p] = combination;
//             } else {
//                 output.input.profileFilterOrigin.dataProfil.values[p] = null;
//             }
//         });
        
//         console.log(JSON.stringify(output, null, 4));
//     });
// });

// // This will handle combinations for two profile values at a time
// for (let i = 0; i < profilValues.length; i++) {
//     for (let j = i + 1; j < profilValues.length; j++) {
//         let firstProfile = profilValues[i];
//         let secondProfile = profilValues[j];

//         let firstValues = profileMappings[firstProfile];
//         let secondValues = profileMappings[secondProfile];

//         if (!firstValues || !secondValues) continue;

//         let firstCombinations = getCombinations(firstValues);
//         let secondCombinations = getCombinations(secondValues);

//         firstCombinations.forEach(firstCombination => {
//             secondCombinations.forEach(secondCombination => {
//                 let output = {
//                     input: {
//                         dataSource: "h1Pemilik",
//                         // ... other static data
//                         profileFilterOrigin: {
//                             // ... other static data within profileFilterOrigin
//                             dataProfil: {
//                                 profilValue: profilValues,
//                                 values: {}
//                             },
//                         },
//                     },
//                 };

//                 output.input.profileFilterOrigin.dataProfil.values[firstProfile] = firstCombination;
//                 output.input.profileFilterOrigin.dataProfil.values[secondProfile] = secondCombination;

//                 profilValues.forEach(p => {
//                     if (!(p in output.input.profileFilterOrigin.dataProfil.values)) {
//                         output.input.profileFilterOrigin.dataProfil.values[p] = null;
//                     }
//                 });

//                 console.log(JSON.stringify(output, null, 4));
//             });
//         });
//     }
// }

const profilValues = [ "pendidikan", "pekerjaan", "agama", "ring","pengeluaran"];
const profileMappings = {
    agama: ["Budha", "Hindu", "Islam", "Katolik", "Konghucu", "Kristen", "LainLain"],
    pengeluaran: ["1", "2", "3", "4", "5", "6", "7"],
    ring: ["0", "1", "2", "3", "4"],
    pekerjaan: ["2", "33", "20", "15", "13", "28", "30", "7", "14", "25", "19", "9", "16", "23", "5", "4", "12", "29", "3", "34", "21", "11", "31", "8", "18", "32", "24", "10", "26", "17", "6", "27", "35", "1", "22"],
    pendidikan: ["TidakTamatSd", "Sd", "Smp", "Smu", "AkademiDiploma", "Sarjana", "PascaSarjana", "TidakBelumSekolah"],
};

// function shuffle(array) {
//     for (let i = array.length - 1; i > 0; i--) {
//         const j = Math.floor(Math.random() * (i + 1));
//         [array[i], array[j]] = [array[j], array[i]]; // Swap elements
//     }
// }

// function combineProfileValues(pValues, index = 0, current = {}) {
//     if (index === pValues.length) return [current];
//     let currentKey = pValues[index];
//     let currentValues = profileMappings[currentKey] ? [...profileMappings[currentKey]] : [null];  // Default to [null] if not in profileMappings

//     // Shuffle values for randomness
//     shuffle(currentValues);

//     let allCombinations = [];

//     for (let value of currentValues) {
//         if (allCombinations.length >= 20) break;
//         let next = { ...current };
//         next[currentKey] = value;
//         allCombinations.push(...combineProfileValues(pValues, index + 1, next));
//     }

//     return allCombinations;
// }

// function combineProfileValues(pValues, index = 0, current = {}) {
//     if (index === pValues.length) return [current];
//     let currentKey = pValues[index];
//     let currentValues = profileMappings[currentKey] || [null];
//     let allCombinations = [];

//     for (let value of currentValues) {
//         if (allCombinations.length >= 20) break;
//         let next = { ...current };
//         next[currentKey] = value;
//         allCombinations.push(...combineProfileValues(pValues, index + 1, next));
//     }

//     return allCombinations;
// }

//========================================================
// let count = 0; // Initialize a counter
// const maxCombinations = 20; 
// function shuffle(array) {
//     for (let i = array.length - 1; i > 0; i--) {
//         const j = Math.floor(Math.random() * (i + 1));
//         [array[i], array[j]] = [array[j], array[i]]; // Swap elements
//     }
// }

// function combineProfileValues(pValues, index = 0, current = {}) {
//     if (index === pValues.length) return [current];
//     let currentKey = pValues[index];
//     let currentValues = profileMappings[currentKey] ? [...profileMappings[currentKey]] : [null];  // Default to [null] if not in profileMappings

//     shuffle(currentValues);

//     let allCombinations = [];
//     for (let value of currentValues) {
//         if (allCombinations.length >= 20) break;
//         let next = { ...current };
//         next[currentKey] = value;
//         allCombinations.push(...combineProfileValues(pValues, index + 1, next));
//     }

//     return allCombinations;
// }

// function getAllSubsets(array) {
//     return array.reduce(
//         (subsets, value) => subsets.concat(
//             subsets.map(set => [value, ...set])
//         ),
//         [[]]
//     );
// }

// const allSubsets = getAllSubsets(profilValues).filter(subset => subset.length > 0);

// function combineProfileValuesForSubsets(subsets) {
//     let allResults = [];
//     for (let subset of subsets) {
//         allResults.push(...combineProfileValues(subset));
//     }
//     return allResults;
// }

// const allProfileCombinations = combineProfileValuesForSubsets(allSubsets);


// allProfileCombinations.forEach((combination, index) => {
//     // Stop when reaching the limit

//     let output = resetObj();
//     let usedKeys = [];
    

//     for(let key of profilValues) {
//         if (combination[key] !== undefined) {
//             output.input.profileFilterOrigin.dataProfil.values[key] = combination[key];
//             usedKeys.push(key);
//         } else {
//             output.input.profileFilterOrigin.dataProfil.values[key] = null;
//         }
//     }
//     output.input.profileFilterOrigin.dataProfil.profilValue = usedKeys;
//     if (count >= maxCombinations) return; 
//     console.log("Result:", JSON.stringify(output, null, 4));
// });
//=======================================================================

let counter = 0;

// function* generateCombinations(arr) {
//     for (let i = 1; i <= arr.length; i++) {
//         yield* getCombinations(arr, i);
//     }
// }

// function* getCombinations(arr, size) {
//     if (!size) {
//         yield [];
//         return;
//     }
//     if (arr.length === size) {
//         yield arr;
//     } else {
//         for (let i = 0; i < arr.length - size + 1; i++) {
//             let head = arr.slice(i, i + 1);
//             let tail = arr.slice(i + 1);
//             for (let tailCombination of getCombinations(tail, size - 1)) {
//                 yield head.concat(tailCombination);
//             }
//         }
//     }
// }


// function setValuesForCombination(combination, mappings, currentProfile = 0, currentValues = {}) {
//     if (currentProfile === combination.length) {
//         let output = resetObj();
//         for (let key of Object.keys(currentValues)) {
//             output.input.profileFilterOrigin.dataProfil.values[key] = currentValues[key];
//         }
//         output.input.profileFilterOrigin.dataProfil.profilValue = combination;
//         console.log("Result:", JSON.stringify(output, null, 4));
//         counter++;
//         return counter >= 20;
//     }

//     let profile = combination[currentProfile];
//     for (let value of mappings[profile]) {
//         if (setValuesForCombination(combination, mappings, currentProfile + 1, { ...currentValues, [profile]: value })) {
//             return true; 
//         }
//     }
//     return false;
// }

// for (let combination of generateCombinations(profilValues)) {
//     if (setValuesForCombination(combination, profileMappings)) {
//         break;
//     }
// }

function resetObj() {
    return {
        
        input: {
            dataSource: "h1Pemilik",
            id: "b5d7b258-1ac0-45a6-dd22-08db93eb462a",
            maxLeads: 10,
            maxLeadsPerDealer: null,
            profileFilterOrigin: {
              sumberData: {
                basisData: "h1Pemilik",
                maxLeads: 10,
                maxLeadsPerDealer: null,
                pilihdata: "LOL",
              },
              dealer: {
                dealerGroup: ["be9f9f5c-9840-ed11-a9b8-8038fbe10c2f"],
                region: ["7aa69f5c-9840-ed11-a9b8-8038fbe10c2f"],
                kabupaten: ["bdc4524a-9840-ed11-a9b8-8038fbe10c2f"],
                dealer: ["b1a39f5c-9840-ed11-a9b8-8038fbe10c2f"],
              },
              filterTanggal: {
                values: [
                  {
                    jenisFilter: "tanggalPembelian",
                    logic: null,
                    periodeTanggal: ["2023-08-01", "2023-08-03"],
                  },
                ],
              },
              dataProfil: {
                profilValue: profilValues,
                values: {
                  agama: null,
                  geografi: {
                    provinsi: ["8bfa5044-9840-ed11-a9b8-8038fbe10c2f"],
                    kecamatan: ["d29d662a-d04a-ed11-a9bc-8038fbe10c2f"],
                    kabupaten: ["9405f315-d04a-ed11-a9bc-8038fbe10c2f"],
                  },
                  jenisKelamin: null,
                  pekerjaan: null,
                  pendidikan: null,
                  pengeluaran: null,
                  tglLahir: ["1990-02-01", "1998-03-05"],
                  usia: {
                    first: 20,
                    last: 60,
                  },
                  ring: null,
                },
              },
              metodePembelian: {
                statusProfil: "Sudah Lunas",
                values: {
                  tenorAwal: 100,
                  tenorAkhir: 200,
                  dpAwal: 2500000,
                  dpAkhir: null,
                  cicilanAwal: 500000,
                  cicilanAkhir: 3000000,
                  metode: ["Cash", "Credit"],
                  fincoy: [
                    "PT. FEDERAL INTERNATIONAL FINANCE",
                    "PT ADIRA DINAMIKA MULTIFINANCE TBK",
                  ],
                  statusPelunasan: {
                    sudahLunas: 36,
                  },
                },
              },
              informasiTambahan: {
                marketSegmen: ["AT HIGH", "AT LOW"],
                series: [
                  "ADV_150_SERIES",
                  "ADV_160_SERIES",
                  "FORZA_SERIES",
                  "GENIO_SERIES",
                  "BEAT_SERIES",
                ],
                tipeUnit: [
                  "3935e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "3b35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "3a35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "3d35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "3c35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "aeb0e533-eb49-ed11-a9bb-8038fbe00b9f",
                  "3e35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "4035e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "3f35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "5235e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "4f35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "5035e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "4d35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "4e35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "4b35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "4c35e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "4935e079-aa40-ed11-a9b8-8038fbe10c2f",
                  "5135e079-aa40-ed11-a9b8-8038fbe10c2f",
                ],
                tahun: "2018",
                unit: 2,
                jenisRo: "MainDealer",
              },
              detailPembelian: {
                jenisKonsumen: ["C", "G", "I", "J"],
                metodePembelian: null,
                konsumenAfPf: null,
                salesAbility: null,
                pembelianUnit: null,
                frekuensiServis: null,
                tahunRakit: null,
                lengkapKPB: null,
                lastToj: null,
                lastMaterialCategory: null,
                rataPembelianAwal: null,
                rataPembelianAkhir: null,
              },
            },
          },
        
    };
}

function getRandomValues(array, count) {
    let shuffled = array.slice(0);
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
    }
    return shuffled.slice(0, count);
}


const generatedResults = [];
const maxResults = 20;






while (generatedResults.length < maxResults) {
    let output = resetObj();

    // Select a random subset of profile keys
    let selectedProfiles = getRandomValues(profilValues, Math.floor(Math.random() * profilValues.length) + 1);
    
    // Initialize all profiles inside values to null
    for (let profile of profilValues) {
        output.input.profileFilterOrigin.dataProfil.values[profile] = null;
    }

    // Populate the selected profiles
    for (let profile of selectedProfiles) {
        if (profileMappings[profile]) { // check if mapping exists
            let valueCount = Math.floor(Math.random() * profileMappings[profile].length) + 1;
            output.input.profileFilterOrigin.dataProfil.values[profile] = getRandomValues(profileMappings[profile], valueCount);
        }
    }

    // Update profilValue to reflect only those keys that are present in selectedProfiles
    output.input.profileFilterOrigin.dataProfil.profilValue = selectedProfiles;

    let outputString = JSON.stringify(output);
    if (!generatedResults.includes(outputString)) { 
        generatedResults.push(outputString);
        console.log("Result:", JSON.stringify(JSON.parse(outputString).input.profileFilterOrigin, null, 4));
    }
}

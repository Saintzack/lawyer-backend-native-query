// src/dto/ContactUsDTO/contactus-messageus.dto.ts

export class ContactUsMessageUsDTO {
  name: string;
  email: string;
  phone_number: string;
  message: string;
  judul: string;
}

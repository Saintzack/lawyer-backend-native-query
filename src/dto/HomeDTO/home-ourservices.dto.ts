export interface HomeOurServicesFilesDTO {
  id: number;
  title: string;
  is_active: boolean;
  description: string;
  created_at: Date;
  updated_at: Date;
  published_at: Date;
  created_by_id: number;
  updated_by_id: number;
  name: string;
  alternative_text: string | null;
  caption: string | null;
  width: number;
  height: number;
  formats: UploadFormats;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  preview_url: string | null;
  provider: string;
  provider_metadata: any | null;
  folder_path: string;
}

 interface UploadFormats {
  small: UploadFormat;
  medium: UploadFormat;
  thumbnail: UploadFormat;
}

 interface UploadFormat {
  ext: string;
  url: string;
  hash: string;
  mime: string;
  name: string;
  path: string | null;
  size: number;
  width: number;
  height: number;
}

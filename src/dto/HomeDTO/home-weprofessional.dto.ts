export interface HomeWeProfessionalDTO2{
    total_klien:number
    kasus_keadilan:number
    tahun_pengalaman:number
}


export interface HomeWeProfessionalDTO{
    id:number
    title_text: string
    description_text: string
    total_klien:number
    kasus_keadilan:number
    tahun_pengalaman:number
    created_at:Date
    updated_at:Date
    published_at:Date
    created_by_id:number
    updated_by_id:number
    image:FileDTO
}

export interface FileDTO {
    id: number;
    name: string;
    alternativeText: string | null;
    caption: string | null;
    width: number;
    height: number;
    formats: {
      small: UploadFormatDTO;
      medium: UploadFormatDTO;
      thumbnail: UploadFormatDTO;
    };
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string | null;
    provider: string;
    provider_metadata: any | null;
    folderPath: string;
    createdAt: Date;
    updatedAt: Date;
    folder: any | null;
  }
  
  export interface UploadFormatDTO {
    ext: string;
    url: string;
    hash: string;
    mime: string;
    name: string;
    path: string | null;
    size: number;
    width: number;
    height: number;
  }


export interface HomeCommendDTO{
    id:number
    description_text: string
    name:string
    pekerjaan:string
    is_active:boolean  
    created_at:Date
    updated_at:Date
    published_at:Date
    created_by_id:number
    updated_by_id:number
    image:FileDTO
}

export interface FileDTO {
    id: number;
    name: string;
    alternativeText: string | null;
    caption: string | null;
    formats: {
      small: UploadFormatDTO;
    };
    previewUrl: string | null;
    provider: string;
    provider_metadata: any | null;
    folderPath: string;
    createdAt: Date;
    updatedAt: Date;
    folder: any | null;
  }
  
  export interface UploadFormatDTO {
    ext: string;
    url: string;
    hash: string;
    mime: string;
    path: string | null;
    size: number;
    width: number;
    height: number;
  }
export interface AboutUsBannerDTO{
    title: string
    description: string
    image: FileDTO
  }
  
  export interface FileDTO {
    id: number;
    name: string;
    alternativeText: string | null;
    caption: string | null;
    format: UploadFormatDTO;
    previewUrl: string | null;
    provider: string;
    provider_metadata: any | null;
    folderPath: string;
    createdAt: Date;
    updatedAt: Date;
    folder: any | null;
  }
  
  export interface UploadFormatDTO {
    ext: string;
    url: string;
    hash: string;
    mime: string;
    name: string;
    path: string | null;
    size: number;
    width: number;
    height: number;
  }
  
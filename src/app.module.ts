import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from './infrastructure/database/Connection/dbConnect';
import { HomeCallExpertModule } from './page/Home/home_callexpert/home_callexpert.module';
import { HomeOurServiceModule } from './page/Home/home_ourservices/home_ourservices.module';
import { HomeCommendModule } from './page/Home/home_commends/home_commends.module';
import { HomeFirstbannerModule } from './page/Home/home_firstbanner/home_firstbanner.module';
import { HomeWeProfessionalModule } from './page/Home/home_weProfessional/home_weProfessional.module';
import { AboutUsBannerModule } from './page/AboutUs/aboutus_banner/aboutus_banner.module';
import { AboutUsHistoryModule } from './page/AboutUs/aboutus_history/aboutus_history.module';
import { AboutUsTeamModule } from './page/AboutUs/aboutus_teams/aboutus_teams.module';
import { ContactUsFirstSectTeamModule } from './page/ContactUs/contactus_firstsection/contactus_firstsection.module';
import { TrainingBannerModule } from './page/Training/training_banners/training_banners.module';
import { TrainingListModule } from './page/Training/training_list_trainings/training_banners.module';
import { LegalsBannerModule } from './page/LegalS/legals-banner/training_legals-banner.module';
import { LegalsListModule } from './page/LegalS/legals-list/legal_list.module';
import { NewsPageModule } from './page/NewsPage/news-page/news_page.module';
import { ContactUsMessageUsModule } from './page/ContactUs/contactus_messageus/contactus_messageus.module';
// import { EmailService } from './email/email.service';

@Module({
  // providers: [EmailService],
  imports: [
    HomeCallExpertModule,
    HomeOurServiceModule,
    HomeCommendModule,
    HomeFirstbannerModule,
    HomeWeProfessionalModule,
    AboutUsBannerModule,
    AboutUsHistoryModule,
    AboutUsTeamModule,
    ContactUsFirstSectTeamModule,
    TrainingBannerModule,
    TrainingListModule,
    LegalsBannerModule,
    LegalsListModule,
    NewsPageModule,
    ContactUsMessageUsModule
    
  ],
  controllers: [],
})
export class AppModule {}

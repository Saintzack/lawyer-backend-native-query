import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { NewsPageDTO } from 'src/dto/NewsPageDTO/news-page.dto';
import { RowDataPacket } from 'mysql2';

@Injectable()
export class NewsPageService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database news-page successfully started');
  }

  async getData(
    category: string,
    limit: number,
    offset: number,
    is_highlights?: number,
    clicked?: number,
    recentDays?: number,
  ): Promise<NewsPageDTO[]> {
    console.log(`Service Limit: ${limit}, Offset: ${offset}`);
    console.log(`recentDays : ${recentDays}`);
  
    const conditions: string[] = ['news_pages.is_active = 1'];
    const values: any[] = [];
  
    if (clicked != null) {
      conditions.push('news_pages.clicked >= ?');
      values.push(clicked);
    }
    conditions.push('(news_pages.category = ? OR ? = \'all\')');
    values.push(category, category);
  
    if (recentDays != null) {
      conditions.push('(news_pages.created_at > DATE_SUB(CURDATE(), INTERVAL ? DAY) OR news_pages.updated_at > DATE_SUB(CURDATE(), INTERVAL ? DAY))');
      values.push(recentDays, recentDays);
    }
  
    try {
      let query = `
        SELECT 
          news_pages.id,
          news_pages.title,
          news_pages.date,
          news_pages.description,
          news_pages.category,
          news_pages.slug,
          JSON_ARRAYAGG(imagez.imagez) AS imagez
        FROM news_pages
        LEFT JOIN (
            SELECT 
                files_related_morphs.related_id AS related_id,
                JSON_OBJECT(
                    'image_name', files.name,
                    'image_alternative_text', files.alternative_text,
                    'image_width', files.width,
                    'image_height', files.height,
                    'image_medium_format', files.formats->'$.medium',
                    'image_hash', files.hash,
                    'image_ext', files.ext,
                    'image_mime', files.mime,
                    'image_size', files.size,
                    'image_url', files.url,
                    'image_provider', files.provider,
                    'data_type', files_related_morphs.field
                ) AS imagez
            FROM files_related_morphs
            INNER JOIN files ON files.id = files_related_morphs.file_id
            WHERE files_related_morphs.field = 'images'
        ) AS imagez ON imagez.related_id = news_pages.id`;
  
      // Join conditions with 'AND'
      if (conditions.length) {
        query += ' WHERE ' + conditions.join(' AND ');
      }
  
      query += `
        GROUP BY 
          news_pages.id, 
          news_pages.title, 
          news_pages.date, 
          news_pages.description, 
          news_pages.category, 
          news_pages.slug
      `;
  
      if (is_highlights != null) {
        query += ` LIMIT 1`;
      } else {
        query += ` LIMIT ? OFFSET ?`;
        values.push(limit, offset); // Push the limit and offset values here
      }
  
      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.query({
        sql: query,
        values: values,
      });
  
      return rows as NewsPageDTO[];
    } catch (error) {
      console.error(error);
      throw new Error('Failed to get news-page data');
    }
  }
  

  async getById(slug: string): Promise<NewsPageDTO> {
    try {
      const query = `
          SELECT 
          news_pages.id AS news_page_id,
      news_pages.title,
      news_pages.date,
      news_pages.description,
      news_pages.category,
      imagez.*
  FROM news_pages
  LEFT JOIN (
    SELECT 
      files_related_morphs.related_id AS related_id,
      JSON_OBJECT(
        
        'image_name', files.name,
        'image_alternative_text', files.alternative_text,
        'image_width', files.width,
        'image_height', files.height,
        'image_medium_format', files.formats->'$.medium',
        'image_hash', files.hash,
        'image_ext', files.ext,
        'image_mime', files.mime,
        'image_size', files.size,
        'image_url', files.url,
        'image_provider', files.provider,
        'data_type', files_related_morphs.field
      ) AS imagez
    FROM files_related_morphs
    INNER JOIN files ON files.id = files_related_morphs.file_id
    WHERE files_related_morphs.field = 'images'
  ) AS imagez ON imagez.related_id = news_pages.id
  WHERE news_pages.slug = ? AND news_pages.is_active = 1;
        `;

      const connection = this.databaseConnectionService.getConnection();
      const [rows] = (await connection.query({
        sql: query,
        values: [slug],
      })) as RowDataPacket[];

      if (rows.length === 0) {
        throw new Error(`No news page found with ID ${slug}`);
      }

      return rows[0] as NewsPageDTO;
    } catch (error) {
      console.error(error);
      throw new Error('Failed to get news-page by ID');
    }
  }

  async postClick(id: number): Promise<void> {
    try {
      const query = `
        UPDATE news_pages
        SET clicked = clicked + 1
        WHERE id = ?
      `;

      const connection = this.databaseConnectionService.getConnection();
      await connection.execute(query, [id]);

      console.log(`Link with ID ${id} was clicked.`);
    } catch (error) {
      console.error(error);
      throw new Error('Failed to post click data');
    }
  }
}

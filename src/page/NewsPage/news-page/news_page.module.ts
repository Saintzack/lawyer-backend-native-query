import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { NewsPageService } from './news_page.service';
import { NewsPageController } from './news_page.controller';

@Module({  
  controllers: [NewsPageController],   
  providers: [NewsPageService,DatabaseConnectionService],
})
export class NewsPageModule {}

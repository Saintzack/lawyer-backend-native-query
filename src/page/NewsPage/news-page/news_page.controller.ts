import { Body, Controller, Get, Post, Query, Res, Param } from '@nestjs/common';
import { Response } from 'express';
import { NewsPageService } from './news_page.service';
// import { IsInt, IsOptional } from 'class-validator';
// import { HomeCallExpertDTO } from 'src/dto/home-callexpert.dto';

@Controller('news-page')
export class NewsPageController {
  constructor(private readonly newsPageService: NewsPageService) {}

  @Get('/')
  async getData(
    @Query('category') category = 'all',
    @Query('limit') limit: number,
    @Query('offset') offset: number,
    @Res() res: Response,
    @Query('is_highlights') is_highlights?: number,
    @Query('clicked') clicked?: number,
    @Query('recentDays') recentDays?: string,
  ): Promise<void> {
    if (recentDays && !this.isInteger(recentDays)) {
      res.status(400).json({ message: 'recentDays should be a valid integer' });
      return;
    }

    try {
      const newsPages = await this.newsPageService.getData(
        String(category),
        Number(limit),
        Number(offset),
        is_highlights,
        clicked,
        recentDays ? Number(recentDays) : undefined,
      );
      const successMessage = 'News data retrieved successfully!';
      res.json({ message: successMessage, data: newsPages });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Failed to get news-page data' });
    }
  }
  isInteger(value: string): boolean {
    return /^\d+$/.test(value);
  }

  @Get('/:slug')
  async getById(@Param('slug') slug: string, @Res() res: Response): Promise<void> {
    try {
      const newsPage = await this.newsPageService.getById(slug);
      res.json({ data: newsPage });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Failed to get news-page by ID' });
    }
  }

  @Post('/click')
  async handleClick(
    @Body() body: { id: number },
    @Res() res: Response,
  ): Promise<void> {
    const successMessage = 'News data retrieved successfully!';
    res.json({ message: successMessage });
    return this.newsPageService.postClick(body.id);
  }
}

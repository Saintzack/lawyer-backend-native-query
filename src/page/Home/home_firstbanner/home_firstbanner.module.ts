import { Module } from '@nestjs/common';
import { HomeFirstbannerService } from './home_firstbanner.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HomeFirstbannerController } from './home_firstbanner.controller'; // Import the controller
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Module({  
  controllers: [HomeFirstbannerController],   
  providers: [HomeFirstbannerService,DatabaseConnectionService],
})
export class HomeFirstbannerModule {}

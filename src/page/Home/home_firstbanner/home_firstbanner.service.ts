import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { HomeFirstBannerDTO } from 'src/dto/HomeDTO/home-firstbanner.dto';

@Injectable()
export class HomeFirstbannerService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database firstbanner successfully started');
  }
  async getData(): Promise<HomeFirstBannerDTO> {
    try {
      const query=`SELECT home_first_banners.*, files.* 
      FROM home_first_banners
      INNER JOIN files_related_morphs on files_related_morphs.file_id
      INNER JOIN files ON files.id = files_related_morphs.file_id
      where files_related_morphs.related_type = 'api::home-first-banner.home-first-banner'
       
      
      `
      const connection = this.databaseConnectionService.getConnection();
      const [rows] =await connection.execute(query)
      return rows[0] as HomeFirstBannerDTO
    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home first banner');
    }
}
}
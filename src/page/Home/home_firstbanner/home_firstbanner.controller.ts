import { Controller,Get } from '@nestjs/common';
import { HomeFirstbannerService } from './home_firstbanner.service';
import { HomeFirstBannerDTO } from 'src/dto/HomeDTO/home-firstbanner.dto';

@Controller()
export class HomeFirstbannerController {
    constructor(private readonly homeFirstBannerService:HomeFirstbannerService){}

    @Get('home-firstbanner')
    async getData():Promise<HomeFirstBannerDTO>{
        return this.homeFirstBannerService.getData()
    }
}

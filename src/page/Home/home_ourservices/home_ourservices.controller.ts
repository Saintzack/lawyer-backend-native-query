import { Controller, Get } from '@nestjs/common';
import { HomeOurServicesService } from './home_ourservices.service';
import { HomeOurServicesFilesDTO } from 'src/dto/HomeDTO/home-ourservices.dto';

@Controller()
export class HomeOurServicesController {
    constructor(private readonly homeOurServicesService: HomeOurServicesService) {}

    @Get('home-ourservices')
    async getData(): Promise<HomeOurServicesFilesDTO[]> {
        return this.homeOurServicesService.getData();
    }
}


import { Test, TestingModule } from '@nestjs/testing';
import { HomeOurServicesController } from './home_ourservices.controller';

describe('HomeFirstbannerController', () => {
  let controller: HomeOurServicesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HomeOurServicesController],
    }).compile();

    controller = module.get<HomeOurServicesController>(HomeOurServicesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

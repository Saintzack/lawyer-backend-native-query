import { Test, TestingModule } from '@nestjs/testing';
import { HomeOurServicesService } from './home_ourservices.service';

describe('HomeFirstbannerService', () => {
  let service: HomeOurServicesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HomeOurServicesService],
    }).compile();

    service = module.get<HomeOurServicesService>(HomeOurServicesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

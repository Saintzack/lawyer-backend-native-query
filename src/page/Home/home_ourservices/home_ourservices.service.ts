import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { HomeOurServicesFilesDTO } from 'src/dto/HomeDTO/home-ourservices.dto';

@Injectable()
export class HomeOurServicesService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database ourservices successfully started');
  }

  async getData(): Promise<HomeOurServicesFilesDTO[]> {

    try {
      const query = `
        SELECT home_ourservices.*, files.*
        FROM home_ourservices
        INNER JOIN files_related_morphs ON files_related_morphs.related_id = home_ourservices.id
        INNER JOIN files ON files.id = files_related_morphs.file_id
        WHERE files_related_morphs.related_type = 'api::home-ourservice.home-ourservice'
        AND home_ourservices.is_active = true
        ORDER BY home_ourservices.title ASC
        LIMIT 10
      `;

      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query);

      // Return the rows directly instead of mapping them
      // console.log(rows);
      
      return rows as HomeOurServicesFilesDTO[];
    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home our services');
    }
  } 
  }




import { Module } from '@nestjs/common';
import { HomeOurServicesService } from './home_ourservices.service';
import { HomeOurServicesController } from './home_ourservices.controller';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Module({
  controllers: [HomeOurServicesController],
  providers: [HomeOurServicesService,DatabaseConnectionService],
})
export class HomeOurServiceModule {}

import { Module } from '@nestjs/common';
import { HomeCallExpertService } from './home_callexpert.service';
import { HomeCallExpertController } from './home_callexpert.controller'; // Import the controller
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Module({  
  
  controllers: [HomeCallExpertController],   
  providers: [HomeCallExpertService,DatabaseConnectionService],
})
export class HomeCallExpertModule {}

import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { HomeCallExpertDTO } from 'src/dto/HomeDTO/home-callexpert.dto';

@Injectable()
export class HomeCallExpertService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database callexpert successfully started');
  }
  async getData(): Promise<HomeCallExpertDTO> {
    try {
      const query = `
      SELECT home_callexperts.title, 
      home_callexperts.description, 
      files.id,
      files.name,
      files.caption,
      JSON_OBJECT(
        'large', files.formats->'$.large',
        'medium', files.formats->'$.medium'
      ) as formats,
      files.url,
      files.preview_url,
      files.provider,
      files.folder_path,
      files.created_at,
      files.updated_at
      FROM home_callexperts
      INNER JOIN files_related_morphs ON files_related_morphs.related_id =home_callexperts.id
      INNER JOIN files ON files.id = files_related_morphs.file_id
      WHERE files_related_morphs.related_type = 'api::home-callexpert.home-callexpert'
      LIMIT 1
`


      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query)

      return rows[0] as HomeCallExpertDTO

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
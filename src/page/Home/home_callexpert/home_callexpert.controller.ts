import { Controller,Get } from '@nestjs/common';
import { HomeCallExpertService} from './home_callexpert.service';
import { HomeCallExpertDTO } from 'src/dto/HomeDTO/home-callexpert.dto';

@Controller()
export class HomeCallExpertController {
    constructor(private readonly homeFirstBannerService:HomeCallExpertService){}

    @Get('home-callexpert')
    async getData():Promise<HomeCallExpertDTO>{
        return this.homeFirstBannerService.getData()
    }
}

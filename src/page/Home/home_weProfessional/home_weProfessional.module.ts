import { Module } from '@nestjs/common';
import { HomeWeProfessionalController } from './home_weProfessional.controller';
import { HomeWeProfessionalService } from './home_weProfessional.service';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Module({
  controllers: [HomeWeProfessionalController], // Corrected the reference to the controller class
  providers: [HomeWeProfessionalService,DatabaseConnectionService],
})
export class HomeWeProfessionalModule {}

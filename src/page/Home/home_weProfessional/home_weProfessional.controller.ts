import { Controller,Get } from '@nestjs/common';
import { HomeWeProfessionalService } from './home_weProfessional.service';
import { HomeWeProfessionalDTO, HomeWeProfessionalDTO2 } from 'src/dto/HomeDTO/home-weprofessional.dto';
// import { HomeWeProfessionals } from 'src/infrastructure/database/mysql/model/homeModel/weProfessional/home-weProfessional.model';

@Controller()
export class HomeWeProfessionalController {
    constructor(private readonly homeWeProfessionalService:HomeWeProfessionalService){}

    @Get('home-weprofessionals')
    async getData():Promise<HomeWeProfessionalDTO>{
        return this.homeWeProfessionalService.getData()
    }
}

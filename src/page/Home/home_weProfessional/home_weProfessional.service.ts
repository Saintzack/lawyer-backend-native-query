import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { HomeWeProfessionalDTO, HomeWeProfessionalDTO2 } from 'src/dto/HomeDTO/home-weprofessional.dto';

@Injectable()
export class HomeWeProfessionalService {
    constructor(
        private readonly databaseConnectionService: DatabaseConnectionService,
    ) {
        console.log('database we professionals successfully started');
    }

    async getData(): Promise<HomeWeProfessionalDTO> {
        try {
            const query = `
            SELECT home_we_professionals.*,files.*
            FROM home_we_professionals
            INNER JOIN files_related_morphs ON files_related_morphs.related_id = home_we_professionals.id
            INNER JOIN files ON files.id = files_related_morphs.file_id
            WHERE files_related_morphs.related_type = 'api::home-we-professional.home-we-professional'
            `

            const connection = this.databaseConnectionService.getConnection();
            const [rows] = await connection.execute(query)
            
            
                      
           return rows[0] as HomeWeProfessionalDTO

        } catch (error) {
            if (error instanceof Error) {
                throw new Error('Failed to get home we professionals');
            } else {
                throw error;
            }
        }
    }
    // async getDataTwo(): Promise<HomeWeProfessionalDTO2[]> {
    //     try {
    //         const query = `
    //         SELECT 
    //         home_we_professionals.total_klien, 
    //         home_we_professionals.tahun_pengalaman,
    //         home_we_professionals.kasus_keadilan
    //         FROM home_we_professionals
    //         LIMIT 1
    //         `

    //         const connection = this.databaseConnectionService.getConnection();
    //         const [rows] = await connection.execute(query)
    //         console.log(rows);
    //        return rows[0] as HomeWeProfessionalDTO[]
    //     } catch (error) {
    //         if (error instanceof Error) {
    //             throw new Error('Failed to get home we professionals');
    //         } else {
    //             throw error;
    //         }
    //     }
    // }
}

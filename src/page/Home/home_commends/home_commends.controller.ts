import { Controller,Get } from '@nestjs/common';
import { HomeCommendService } from './home_commends.service';
import { HomeCommendDTO } from 'src/dto/HomeDTO/home-commend.dto';

@Controller()
export class HomeCommendController {
    constructor(private readonly homeCommendService:HomeCommendService){}

    @Get('home-commend')
    async getData():Promise<HomeCommendDTO[]>{
        return this.homeCommendService.getData()
    }
}

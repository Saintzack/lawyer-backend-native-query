import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { HomeCommendDTO } from 'src/dto/HomeDTO/home-commend.dto';
import { EntityNotFoundError, Repository } from 'typeorm';

@Injectable()
export class HomeCommendService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database commends successfully started');
  }

  async getData(): Promise<HomeCommendDTO[]> {
    try {
      const query = `
        SELECT home_commends.*,files.*
        FROM home_commends
        INNER JOIN files_related_morphs ON files_related_morphs.related_id = home_commends.id
        INNER JOIN files ON files.id = files_related_morphs.file_id
        WHERE files_related_morphs.related_type = 'api::home-commend.home-commend'
        AND home_commends.is_active = 1
        ORDER BY home_commends.created_at ASC
        LIMIT 10
      `;

      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query);

      // Return the rows directly instead of mapping them
      return rows as HomeCommendDTO[];
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        // handle the error here
      } else {
        throw error;
      }
    }
  }}
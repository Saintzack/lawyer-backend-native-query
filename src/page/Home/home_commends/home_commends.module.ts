import { Module } from '@nestjs/common';
import { HomeCommendService } from './home_commends.service';
import { HomeCommendController } from './home_commends.controller'; // Import the controller
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Module({
  controllers: [HomeCommendController], // Corrected the reference to the controller class
  providers: [HomeCommendService,DatabaseConnectionService],
})
export class HomeCommendModule {}

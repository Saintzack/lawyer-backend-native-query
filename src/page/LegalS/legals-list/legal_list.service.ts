import { Injectable } from '@nestjs/common';
import { LegalsBannerDTO } from 'src/dto/LegalsDTO/legals-banner.dto';
import { LegalsListDTO } from 'src/dto/LegalsDTO/legals-list.dto';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Injectable()
export class LegalsListService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database legals-list successfully started');
  }

  async getData(offset: number, limit: number): Promise<LegalsListDTO[]> {
    try {
      const query = `
      SELECT legal_service_our_products.*, 
       GROUP_CONCAT(files.id) as file_ids, 
       GROUP_CONCAT(files.url) as file_urls, 
       GROUP_CONCAT(files.mime) as file_mimes
FROM legal_service_our_products
INNER JOIN files_related_morphs ON files_related_morphs.related_id = legal_service_our_products.id
INNER JOIN files ON files.id = files_related_morphs.file_id
WHERE files_related_morphs.related_type = 'api::legal-service-our-product.legal-service-our-product'
  and is_active = 1 
GROUP BY legal_service_our_products.id
ORDER BY legal_service_our_products.updated_at DESC
LIMIT ?, ?

      
`;
      console.log('Offset:', offset); // <-- Add logging here
      console.log('Limit:', limit); // <-- Add logging here

      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.query(query, [offset, limit]);

      

      return rows as LegalsListDTO[];
    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}

import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { LegalsListController } from './legal_list.controller';
import { LegalsBannerService } from '../legals-banner/training_legals-banner.service';
import { LegalsListService } from './legal_list.service';

@Module({  
  controllers: [LegalsListController],   
  providers: [LegalsListService,DatabaseConnectionService],
})
export class LegalsListModule {}

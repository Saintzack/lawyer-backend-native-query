import { Controller,Get,Query } from '@nestjs/common';
import { LegalsListService } from './legal_list.service';
import { LegalsListDTO } from 'src/dto/LegalsDTO/legals-list.dto';

@Controller()
export class LegalsListController {
  constructor(private readonly legalsListService: LegalsListService) {}

  @Get('legal-list')
  async getData(
    @Query('offset') offset: string = '0',
    @Query('limit') limit: string = '10',
  ): Promise<LegalsListDTO[]> {
    return this.legalsListService.getData(Number(offset), Number(limit));
  }
}
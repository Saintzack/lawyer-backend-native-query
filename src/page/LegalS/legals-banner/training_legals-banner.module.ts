import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { LegalsBannerController } from './training_legals-banner.controller';
import { LegalsBannerService } from './training_legals-banner.service';

@Module({  
  controllers: [LegalsBannerController],   
  providers: [LegalsBannerService,DatabaseConnectionService],
})
export class LegalsBannerModule {}

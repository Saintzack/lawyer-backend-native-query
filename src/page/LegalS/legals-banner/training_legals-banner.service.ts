import { Injectable } from '@nestjs/common';
import { LegalsBannerDTO } from 'src/dto/LegalsDTO/legals-banner.dto';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Injectable()
export class LegalsBannerService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database legal-banner successfully started');
  }
  async getData(): Promise<LegalsBannerDTO> {
    try {
      const query = `
      SELECT legal_service_banners.*, files.*
        FROM legal_service_banners
        INNER JOIN files_related_morphs ON files_related_morphs.related_id = legal_service_banners.id
        INNER JOIN files ON files.id = files_related_morphs.file_id
        WHERE files_related_morphs.related_type = 'api::legal-service-banner.legal-service-banner'
      
`


      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query)

      return rows[0] as LegalsBannerDTO

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
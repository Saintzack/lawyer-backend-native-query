import { Controller,Get } from '@nestjs/common';
import { LegalsBannerService } from './training_legals-banner.service';
import { LegalsBannerDTO } from 'src/dto/LegalsDTO/legals-banner.dto';

@Controller()
export class LegalsBannerController {
    constructor(private readonly homeFirstBannerService:LegalsBannerService){}

    @Get('legal-service')
    async getData():Promise<LegalsBannerDTO>{
        return this.homeFirstBannerService.getData()
    }
}

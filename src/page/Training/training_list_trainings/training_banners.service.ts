import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { TrainingBannerDTO } from 'src/dto/TrainingDTO/training-banner.dto';
import { TrainingListDTO } from 'src/dto/TrainingDTO/training-list.dto';
@Injectable()
export class TrainingListService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database training-list successfully started');
  }
  async getData(offset:number, limit: number): Promise<TrainingListDTO[]> {
    try {
      const query = `
      SELECT training_list_trainings.*, 
       GROUP_CONCAT(files.id) as file_ids, 
       GROUP_CONCAT(files.url) as file_urls, 
       GROUP_CONCAT(files.mime) as file_mimes
FROM training_list_trainings
INNER JOIN files_related_morphs ON files_related_morphs.related_id = training_list_trainings.id
INNER JOIN files ON files.id = files_related_morphs.file_id
WHERE files_related_morphs.related_type = 'api::training-list-training.training-list-training'
  and is_active = 1 
GROUP BY training_list_trainings.id
ORDER BY training_list_trainings.updated_at DESC
LIMIT ?, ?

`


const connection = this.databaseConnectionService.getConnection();
const [rows] = await connection.query(query, [offset, limit]);

return rows as TrainingListDTO[]

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
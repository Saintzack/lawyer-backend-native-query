import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { TrainingListController } from './training_banners.controller';
import { TrainingListService } from './training_banners.service';

@Module({  
  controllers: [TrainingListController],   
  providers: [TrainingListService,DatabaseConnectionService],
})
export class TrainingListModule {}

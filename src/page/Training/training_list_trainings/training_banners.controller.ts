import { Controller,Get,Query } from '@nestjs/common';
import { TrainingListService } from './training_banners.service';
import { TrainingListDTO } from 'src/dto/TrainingDTO/training-list.dto';
// import { HomeCallExpertDTO } from 'src/dto/home-callexpert.dto';

@Controller()
export class TrainingListController {
    constructor(private readonly trainingListService:TrainingListService){}

    @Get('training-list')
  async getData(
    @Query('offset') offset: string = '0',
    @Query('limit') limit: string = '10',
  ): Promise<TrainingListDTO[]> {
    return this.trainingListService.getData(Number(offset), Number(limit));
  }
}

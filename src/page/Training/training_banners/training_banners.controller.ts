import { Controller,Get } from '@nestjs/common';
import { TrainingBannerService } from './training_banners.service';
import { ContactUsFirstSectDTO } from 'src/dto/ContactUsDTO/contactus-firstsect.dto';
import { TrainingBannerDTO } from 'src/dto/TrainingDTO/training-banner.dto';
// import { HomeCallExpertDTO } from 'src/dto/home-callexpert.dto';

@Controller()
export class TrainingBannerController {
    constructor(private readonly homeFirstBannerService:TrainingBannerService){}

    @Get('training-banner')
    async getData():Promise<TrainingBannerDTO>{
        return this.homeFirstBannerService.getData()
    }
}

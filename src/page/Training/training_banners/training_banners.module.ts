import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { TrainingBannerController } from './training_banners.controller';
import { TrainingBannerService } from './training_banners.service';

@Module({  
  controllers: [TrainingBannerController],   
  providers: [TrainingBannerService,DatabaseConnectionService],
})
export class TrainingBannerModule {}

import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { TrainingBannerDTO } from 'src/dto/TrainingDTO/training-banner.dto';
@Injectable()
export class TrainingBannerService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database training-banners successfully started');
  }
  async getData(): Promise<TrainingBannerDTO> {
    try {
      const query = `
      SELECT training_banners.*, files.*
        FROM training_banners
        INNER JOIN files_related_morphs ON files_related_morphs.related_id = training_banners.id
        INNER JOIN files ON files.id = files_related_morphs.file_id
        WHERE files_related_morphs.related_type = 'api::training-banner.training-banner'
      
`


      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query)

      return rows[0] as TrainingBannerDTO

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
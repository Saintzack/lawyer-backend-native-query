import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { AboutUsTeamDTO } from 'src/dto/AboutUsDTO/aboutus-team.dto';
import { ContactUsFirstSectDTO } from 'src/dto/ContactUsDTO/contactus-firstsect.dto';
import { ContactUsMessageUsDTO } from 'src/dto/ContactUsDTO/contactus-messageus.dto';
// import { EmailService } from 'src/email/email.service';
import * as dotenv from 'dotenv';
dotenv.config();

@Injectable()
export class ContactUsMessageUsService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
    // private readonly emailService: EmailService,
  ) {
    console.log('database messages successfully started');
  }
  async postMessage(
    messageData: ContactUsMessageUsDTO,
  ): Promise<ContactUsMessageUsDTO> {
    try {
      const query = `
            INSERT INTO messages (name, email, phone_number, message, judul)
            VALUES (?, ?, ?, ?, ?)
        `;

      const connection = this.databaseConnectionService.getConnection();
      await connection.execute(query, [
        messageData.name,
        messageData.email,
        messageData.phone_number,
        messageData.message,
        messageData.judul,
      ]);

      // After successfully inserting into the database, send email:
      // const emailRecipient = 'default_email@example.com'; // replace with the desired recipient's email
      // const emailSubject = 'New Message from Contact Us Form';
      // const emailText = `Name: ${messageData.name}\nEmail: ${messageData.email}\nPhone: ${messageData.phone_number}\nTitle: ${messageData.judul}\nMessage: ${messageData.message}`;

      // await this.emailService.sendMail(emailRecipient, emailSubject, emailText);

      return messageData;
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to post message data: ${error.message}`);

    }
  }
}

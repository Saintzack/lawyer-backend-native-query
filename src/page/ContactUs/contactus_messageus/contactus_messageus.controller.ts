import { Controller,Post,Body } from '@nestjs/common';
import { ContactUsMessageUsService } from './contactus_messageus.service';
import { ContactUsFirstSectDTO } from 'src/dto/ContactUsDTO/contactus-firstsect.dto';
import { ContactUsMessageUsDTO } from 'src/dto/ContactUsDTO/contactus-messageus.dto';
// import { HomeCallExpertDTO } from 'src/dto/home-callexpert.dto';

@Controller()
export class ContactUsMessageUsController {
    constructor(private readonly homeFirstBannerService:ContactUsMessageUsService){}

    @Post('contactus-messageus')
    async postData(@Body() messageData: ContactUsMessageUsDTO): Promise<ContactUsMessageUsDTO> {
        return this.homeFirstBannerService.postMessage(messageData);
    }
}

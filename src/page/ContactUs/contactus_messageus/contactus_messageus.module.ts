import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { ContactUsMessageUsController } from './contactus_messageus.controller';
import { ContactUsMessageUsService } from './contactus_messageus.service';
// import { EmailService } from 'src/email/email.service';

@Module({  
  controllers: [ContactUsMessageUsController],   
  providers: [ContactUsMessageUsService,DatabaseConnectionService],
})
export class ContactUsMessageUsModule {}

import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { AboutUsTeamDTO } from 'src/dto/AboutUsDTO/aboutus-team.dto'; 
import { ContactUsFirstSectDTO } from 'src/dto/ContactUsDTO/contactus-firstsect.dto';
@Injectable()
export class ContactUsFirstSectService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database contactus-firstsection successfully started');
  }
  async getData(): Promise<ContactUsFirstSectDTO> {
    try {
      const query = `
      SELECT contact_us_first_sections.*, files.*
        FROM contact_us_first_sections
        INNER JOIN files_related_morphs ON files_related_morphs.related_id = contact_us_first_sections.id
        INNER JOIN files ON files.id = files_related_morphs.file_id
        WHERE files_related_morphs.related_type = 'api::contact-us-first-section.contact-us-first-section'
        limit 1
      
`


      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query)

      return rows[0] as ContactUsFirstSectDTO

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
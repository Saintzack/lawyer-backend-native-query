import { Module } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { ContactUsFirstSectService } from './contactus_firstsection.service';
import { ContactUsFirstSectController } from './contactus_firstsection.controller';

@Module({  
  controllers: [ContactUsFirstSectController],   
  providers: [ContactUsFirstSectService,DatabaseConnectionService],
})
export class ContactUsFirstSectTeamModule {}

import { Controller,Get } from '@nestjs/common';
import { ContactUsFirstSectService } from './contactus_firstsection.service';
import { ContactUsFirstSectDTO } from 'src/dto/ContactUsDTO/contactus-firstsect.dto';
// import { HomeCallExpertDTO } from 'src/dto/home-callexpert.dto';

@Controller()
export class ContactUsFirstSectController {
    constructor(private readonly homeFirstBannerService:ContactUsFirstSectService){}

    @Get('contactus-firstsect')
    async getData():Promise<ContactUsFirstSectDTO>{
        return this.homeFirstBannerService.getData()
    }
}

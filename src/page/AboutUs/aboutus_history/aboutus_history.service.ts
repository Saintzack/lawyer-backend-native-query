import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { AboutUsHistoryDTO } from 'src/dto/AboutUsDTO/aboutus-history.dto';

@Injectable()
export class AboutUsHistoryService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database about_us_histories successfully started');
  }
  async getData(): Promise<AboutUsHistoryDTO[]> {
    try {
      const query = `
      SELECT about_us_histories.title, 
      about_us_histories.description, 
      files.id,
      files.name,
      files.caption,
      JSON_OBJECT(
        'large', files.formats->'$.large',
        'medium', files.formats->'$.medium'
      ) as formats,
      files.preview_url,
      files.url,
      files.provider,
      files.folder_path,
      files.created_at,
      files.updated_at
      FROM about_us_histories
      INNER JOIN files_related_morphs ON files_related_morphs.related_id =about_us_histories.id
      INNER JOIN files ON files.id = files_related_morphs.file_id
      WHERE files_related_morphs.related_type = 'api::about-us-history.about-us-history'
      LIMIT 1
      `


      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query)

      return rows[0] as AboutUsHistoryDTO[]

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
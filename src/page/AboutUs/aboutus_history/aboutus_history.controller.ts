import { Controller,Get } from '@nestjs/common';
import {  AboutUsHistoryService} from './aboutus_history.service';
import { AboutUsHistoryDTO } from 'src/dto/AboutUsDTO/aboutus-history.dto';

@Controller()
export class AboutUsHistoryController {
    constructor(private readonly homeFirstBannerService:AboutUsHistoryService){}

    @Get('aboutus-history')
    async getData():Promise<AboutUsHistoryDTO[]>{
        return this.homeFirstBannerService.getData()
    }
}

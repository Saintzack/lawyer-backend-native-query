import { Module } from '@nestjs/common';
import { AboutUsHistoryController } from './aboutus_history.controller';
import { AboutUsHistoryService } from './aboutus_history.service';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
@Module({  
  controllers: [AboutUsHistoryController],   
  providers: [AboutUsHistoryService,DatabaseConnectionService],
})
export class AboutUsHistoryModule {}

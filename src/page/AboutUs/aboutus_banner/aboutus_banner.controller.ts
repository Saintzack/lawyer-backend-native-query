import { Controller,Get } from '@nestjs/common';
import { AboutUsBannerService} from './aboutus_banner.service';
import { AboutUsBannerDTO } from 'src/dto/AboutUsDTO/aboutus-banner.dto';
// import { HomeCallExpertDTO } from 'src/dto/home-callexpert.dto';

@Controller()
export class AboutUsBannerController {
    constructor(private readonly homeFirstBannerService:AboutUsBannerService){}

    @Get('aboutus-banners')
    async getData():Promise<AboutUsBannerDTO[]>{
        return this.homeFirstBannerService.getData()
    }
}

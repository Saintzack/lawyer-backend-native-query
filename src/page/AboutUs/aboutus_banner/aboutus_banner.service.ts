import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { AboutUsBannerDTO } from 'src/dto/AboutUsDTO/aboutus-banner.dto'; 

@Injectable()
export class AboutUsBannerService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database aboutusbanners successfully started');
  }
  async getData(): Promise<AboutUsBannerDTO[]> {
    try {
      const query = `
      SELECT about_us_banners.title, 
      about_us_banners.description, 
      files.id,
      files.name,
      files.caption,
      JSON_OBJECT(
        'large', files.formats->'$.large',
        'medium', files.formats->'$.medium'
      ) as formats,
      files.preview_url,
      files.url,
      files.provider,
      files.folder_path,
      files.created_at,
      files.updated_at
      FROM about_us_banners
      INNER JOIN files_related_morphs ON files_related_morphs.related_id =about_us_banners.id
      INNER JOIN files ON files.id = files_related_morphs.file_id
      WHERE files_related_morphs.related_type = 'api::about-us-banner.about-us-banner'
      LIMIT 1
`


      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query)

      return rows[0] as AboutUsBannerDTO[]

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
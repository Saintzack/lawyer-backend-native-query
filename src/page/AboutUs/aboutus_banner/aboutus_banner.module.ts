import { Module } from '@nestjs/common';
import { AboutUsBannerService } from './aboutus_banner.service';
import { AboutUsBannerController } from './aboutus_banner.controller'; // Import the controller
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Module({  
  controllers: [AboutUsBannerController],   
  providers: [AboutUsBannerService,DatabaseConnectionService],
})
export class AboutUsBannerModule {}

import { Module } from '@nestjs/common';
import { AboutUsTeamController } from './aboutus_teams.controller';
import { AboutUsTeamService } from './aboutus_teams.service';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';

@Module({  
  controllers: [AboutUsTeamController],   
  providers: [AboutUsTeamService,DatabaseConnectionService],
})
export class AboutUsTeamModule {}

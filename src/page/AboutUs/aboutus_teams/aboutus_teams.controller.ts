import { Controller,Get } from '@nestjs/common';
import { AboutUsTeamService} from './aboutus_teams.service';
import { AboutUsTeamDTO } from 'src/dto/AboutUsDTO/aboutus-team.dto';
// import { HomeCallExpertDTO } from 'src/dto/home-callexpert.dto';

@Controller()
export class AboutUsTeamController {
    constructor(private readonly homeFirstBannerService:AboutUsTeamService){}

    @Get('aboutus-team')
    async getData():Promise<AboutUsTeamDTO[]>{
        return this.homeFirstBannerService.getData()
    }
}

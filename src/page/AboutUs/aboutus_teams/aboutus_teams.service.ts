import { Injectable } from '@nestjs/common';
import { DatabaseConnectionService } from 'src/infrastructure/database/Connection/dbConnect';
import { AboutUsTeamDTO } from 'src/dto/AboutUsDTO/aboutus-team.dto'; 
@Injectable()
export class AboutUsTeamService {
  constructor(
    private readonly databaseConnectionService: DatabaseConnectionService,
  ) {
    console.log('database aboutusteam successfully started');
  }
  async getData(): Promise<AboutUsTeamDTO[]> {
    try {
      const query = `
      SELECT about_us_teams.name, 
      about_us_teams.jabatan, 
      about_us_teams.experience, 
      about_us_teams.is_active       
      FROM about_us_teams
      WHERE is_active = true
`


      const connection = this.databaseConnectionService.getConnection();
      const [rows] = await connection.execute(query)

      return rows as AboutUsTeamDTO[]

    } catch (error) {
      console.error(error);
      throw new Error('Failed to get home data');
    }
  }
}
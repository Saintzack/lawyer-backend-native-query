import { Injectable } from "@nestjs/common";
import * as mysql from 'mysql2/promise';
import * as dotenv from 'dotenv';

dotenv.config();
@Injectable()
export class DatabaseConnectionService {
  private _connection: mysql.Connection;

  constructor() {
    this._initializeConnection();
  }

  private async _initializeConnection() {
    this._connection = await mysql.createConnection({
      host: '127.0.0.1',
      user: 'root',
      password: process.env.PASSWORD,
      database: process.env.DATABASE,
    });
  }

  public getConnection(): mysql.Connection {
    return this._connection;
  }
}

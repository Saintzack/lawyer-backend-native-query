// import { Files } from "../../files-strapi-firstbanner";
import { FilesV2 } from "../files/files.model";
// import { HomeOurServices } from "../../homeModel/ourServices/home-ourservices.model";
import { HomeOurServicesV2 } from "../home.ourservices/home.ourservices.model";

export interface FilesMorphV2 {
    id: number;
    filez: FilesV2;
    servicez: HomeOurServicesV2;
    // commendz: HomeCommend;
    // professionalz: HomeWeProfessionals;
    related_type: string;
    field: number;
    order: number;
  }
  
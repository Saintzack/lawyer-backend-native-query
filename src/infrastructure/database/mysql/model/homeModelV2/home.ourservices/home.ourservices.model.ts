// import { FilesMorph } from "../../homeModel/ourServices/files-morph";
import { FilesMorphV2 } from "../filesmorph/filesMorph";

export interface HomeOurServicesV2 {
    id: number;
    title: string;
    is_active: boolean;
    description: string;
    created_at: Date;
    updated_at: Date;
    published_at: Date;
    created_by_id: number;
    updated_by_id: number;
    filesMorphs: FilesMorphV2[];
  }
  
import { FilesMorphV2 } from "../filesmorph/filesMorph";
import { UploadFormats } from "./uploadFormat.model";

export interface FilesV2 {
    id: number;
    name: string;
    alternative_text: string | null;
    caption: string | null;
    width: number;
    height: number;
    formats: UploadFormats;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    preview_url: string | null;
    provider: string;
    provider_metadata: any | null;
    folder_path: string;
    created_at: Date;
    updated_at: Date;
    created_by_id: number;
    updated_by_id: number;
    filesMorphz: FilesMorphV2[];
  }
  
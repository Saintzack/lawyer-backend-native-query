export interface UploadFormats {
    small: UploadFormat;
    medium: UploadFormat;
    thumbnail: UploadFormat;
  }
  
  export interface UploadFormat {
    ext: string;
    url: string;
    hash: string;
    mime: string;
    name: string;
    path: string | null;
    size: number;
    width: number;
    height: number;
  }
  